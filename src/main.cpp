#include <stdio.h>
#include <string>
#include <math.h> 
#include <iostream>
#include <sferes/phen/parameters.hpp>
#include <sferes/gen/evo_float.hpp>
#include <sferes/ea/nsga2.hpp>
#include <sferes/stat/pareto_front.hpp>
#include <sferes/modif/dummy.hpp>
#include <sferes/run.hpp>
#include <boost/program_options.hpp>

#define GRAPHIC
#ifdef GRAPHIC
#define NO_PARALLEL
#endif

#ifndef NO_PARALLEL
#include <sferes/eval/parallel.hpp>
#else
#include <sferes/eval/eval.hpp>
#endif

using namespace sferes;
using namespace sferes::gen::evo_float;

#include "learning_params.hpp"
#include <RobotFramework/api/logging.hpp>
#include <RobotFramework/api/utils.hpp>
#include "env/robotDart.hpp"
#include "parameterReader.hpp"

Eigen::Vector2f costFunction(const Eigen::VectorXf &param_);

// CONF PARAMETERS
float dt_ = 0.01;
std::string robotName_ = "icub_description";
std::string packageName_ = "icub_description";
std::string urdf_ = "/nfs/hal01/amacri/icub_description/urdf/icub.urdf";
//std::string urdf_ = "/nfs/hal01/amacri/icub_description/urdf/icub_common_simplified_collision_without_mesh.urdf";
std::string packagePath_ = "/nfs/hal01/amacri/icub_description";
std::string configFile_ = "/nfs/hal01/amacri/icub_description/etc/config_icub_floating_base.yaml";
    
Eigen::VectorXd q0_(38); 
Eigen::VectorXd dq0_ = Eigen::VectorXd::Zero(q0_.size());
Eigen::VectorXf fitness_(2); 
bool isLearning = true;

namespace sferes {
    namespace stat {
        SFERES_STAT(ParetoSamples, Stat)
        {
        public:
            typedef std::vector<boost::shared_ptr<Phen> > pareto_t;
            // assume a ea.pareto_front() method
            template <typename E>
            void refresh(const E& ea)
            {
                _pareto_front = ea.pareto_front();
                parallel::sort(_pareto_front.begin(), _pareto_front.end(),
                    fit::compare_objs_lex());
                this->_create_log_file(ea, "pareto_samples.dat");
                if (ea.dump_enabled())
                    show_all(*(this->_log_file), ea.gen(), ea.nb_evals());
                //this->_log_file->close();
            }
            void show(std::ostream & os, size_t k) const
            {
                os << "log format : gen nb_evals id sample_1 ... sample_n" << std::endl;
                show_all(os, 0);
                _pareto_front[k]->develop();
                _pareto_front[k]->show(os);
                _pareto_front[k]->fit().set_mode(fit::mode::view);
                _pareto_front[k]->fit().eval(*_pareto_front[k]);
                os << "=> displaying individual " << k << std::endl;
                os << "samples:";
                for (unsigned j = 0; j < _pareto_front[k]->data().size(); ++j)
                    os << _pareto_front[k]->data(j) << " ";
                os << std::endl;
                assert(k < _pareto_front.size());
            }
            const pareto_t& pareto_front() const
            {
                return _pareto_front;
            }
            template <class Archive>
            void serialize(Archive & ar, const unsigned int version)
            {
                ar& BOOST_SERIALIZATION_NVP(_pareto_front);
            }

            void show_all(std::ostream & os, size_t gen = 0, size_t nb_evals = 0) const
            {
                for (unsigned i = 0; i < _pareto_front.size(); ++i) {
                    os << gen << " " << nb_evals << " " << i << " ";
                    for (unsigned j = 0; j < _pareto_front[i]->data().size(); ++j)
                        os << _pareto_front[i]->data(j) << " ";
                    os << std::endl;
                }
            }

        protected:
            pareto_t _pareto_front;
        };
    }
}

template<typename Indiv>
Eigen::VectorXf _g(const Indiv &ind) {
  Eigen::VectorXf g;
  g.resize(20);
  for (size_t i = 1; i < 20; ++i)
    g(i) = ind.data(i);
  return g;
}

SFERES_FITNESS(FitZDT2, sferes::fit::Fitness) {
public:
  FitZDT2()  {}
  template<typename Indiv>
  void eval(Indiv& ind) {
    Eigen::Vector2f fit_ = costFunction(_g(ind));
    this->_objs.resize(2);
    this->_objs[0] = -fit_(0);
    this->_objs[1] = -fit_(1);
  }
};

int main(int argc, char **argv) {
    std::cout<<"Running "<<argv[0]<<" ... try --help for options (verbose)"<<std::endl;
q0_ << 0.0, 0.0, 0.4643, 0.0, 0.0, 0.0,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.087, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749,
          0.0171127, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749;

    // RUN OPTIMIZATION
    #ifndef NO_PARALLEL
        typedef eval::Parallel<Params> eval_t;
    #else
        typedef eval::Eval<Params> eval_t;
    #endif

    typedef gen::EvoFloat<20, Params> gen_t;
    typedef phen::Parameters<gen_t, FitZDT2<Params>, Params> phen_t;
    typedef boost::fusion::vector<stat::ParetoFront<phen_t, Params>, stat::ParetoSamples<phen_t, Params> >  stat_t;
    typedef modif::Dummy<> modifier_t;
    typedef ea::Nsga2<phen_t, eval_t, stat_t, modifier_t, Params> ea_t;
    ea_t ea;

    if (isLearning) {
        run_ea(argc, argv, ea);
    } else {
        Eigen::VectorXf param_;
        Eigen::MatrixXf solution_data;
        Eigen::VectorXf solution_line;
        param_.resize(20);

        std::string simulation = "0";
        mkdir(("simulation_"+ simulation).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        // parameterReader reader("./learned_parameters_with_torques/learned_parameters_"+ simulation +".txt");
        // solution_data = (reader.getData()).cast <float> ();
    
        // for (int line=11; line<solution_data.rows()-1; line=line+100){
        // //    for (int line=32; line<solution_data.rows()-1; line=line+100){
        //          solution_line = solution_data.row(line);
        //          param_ = solution_line.tail(20);


        for (int line=0; line<1; line++){
            param_ << 1.0, 0.1, 0.1, 0.1, 0.1, 0.5, 
                    1, 1, 1, 1, 1, 0.1, 1,
                    0, 0.5, 0, 0.5, 0.5, 0.5, 0.5;

            bool control_init_ = false;
            env::RobotDart rd(configFile_, dt_, robotName_, urdf_, packageName_, packagePath_, q0_, dq0_, param_);
            try{
                rd.initQPController();
                //LOG_SUCCESS("Initialized controller");
                control_init_ = true;
            }
            catch (...){
                //LOG_ERROR("Controller initialization failed. Unfeasible autostack");
                control_init_ = false;
            }

            
            if (control_init_){
                #ifndef NO_PARALLEL
                    rd.initSimulator(true);
                #else
                    rd.initSimulator(true);
                #endif
        
                LOG_SUCCESS("Initialized simulator");
                // RUN SIMULATION AND GET COST
                std::cout<<"Running configuration " << simulation << "x" << line << std::endl;
                rd.runEnv(line, simulation);
                fitness_ = rd.getScore();
            }
        
        }
    }

    return 0;
}


Eigen::Vector2f costFunction(const Eigen::VectorXf &param_){
    bool control_init_ = false;
    env::RobotDart rd(configFile_, dt_, robotName_, urdf_, packageName_, packagePath_, q0_, dq0_, param_);
    try{
        rd.initQPController();
        //LOG_SUCCESS("Initialized controller");
        control_init_ = true;
    }
    catch (...){
        //LOG_ERROR("Controller initialization failed. Unfeasible autostack");
        control_init_ = false;
    }

    
    if (control_init_){
        #ifndef NO_PARALLEL
            rd.initSimulator(false);
        #else
            rd.initSimulator(false);
        #endif

        LOG_SUCCESS("Initialized simulator");
        // RUN SIMULATION AND GET COST
        std::cout<<"Running" <<std::endl;
        rd.runEnv(-1, " ");
        }

    return rd.getScore();
}