#include "control/icub_learning_controller.hpp"

RF_REGISTER_CONTROLLER("icub_learning_DS", control::icub_learning_controller);

control::icub_learning_controller::icub_learning_controller(
 const std::string& confFile, double dt) : opensot_controller(confFile, dt),
 trjGeneratorC_(std::make_shared<trajectory_utils::trajectory_generator>(dt)),
 trjGeneratorF_(std::make_shared<trajectory_utils::trajectory_generator>(dt)),
 trjGeneratorN_(std::make_shared<trajectory_utils::trajectory_generator>(dt))
{
  //set init joint angles (includes floating base)
  // 0:5 floating base, 6:11 left leg, 12:17 right leg, 18:20 torso, 21:26 left arm, 27:30 neck, 31:36 right arm
  q0_ << 0.0, 0.0, 0.4643, 0.0, 0.0, 0.0,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.087, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749,
          0.0171127, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749; 
         
      //virtual joints (x5)
      // hip_p, hip_r, hip_y, knee, ankle_p, ankle_r
      // torso_p, torso_r, torso_y
      //shoulder_p, shoulder_r, shoulder_y, elbow, wrist_pr, wrist_p, wrist_y
      //neck_p, neck_r, neck_p
  
  logs_ = std::make_shared<api::loggers>(jNames_);
  
  //control param
  lambda.resize(6);
  // w.resize(8);
  // taskSel.resize(8);
  // taskAct_1.resize(8);
  // taskAct_2.resize(8);
  // taskAct_3.resize(8);
  w.resize(7);
  taskSel.resize(7);
  taskAct_1.resize(7);
  taskAct_2.resize(7);
  taskAct_3.resize(7);

  com_ref_.resize(3);
  r_foot_ref_.resize(3);
  l_foot_ref_.resize(3);
  //LOG_INFO("Controller icub_learning_controller constructed");

  duration_limit = 2000;
  y_com_d = 0.04;
  x_foot_d = 0.03;
  y_foot_d = 0.02;
  z_foot_d = 0.075;

  // always time_1 < time_2
  time_1 = 8.0;
  time_2 = 10.0;
 

}

control::icub_learning_controller::~icub_learning_controller() {
  //LOG_WARNING("Controller icub_learning_controller destructed");
}

void control::icub_learning_controller::setCtrlParameters(const Eigen::VectorXf &opt_param){ 
  // DEFINE MAPPING OPTIMIZER PARAMETERS - CONTROL PARAMETERS
    for (int i = 0; i < lambda.size(); i++) {
      lambda(i)=opt_param(i);
    }
    for (int i = 0; i < w.size(); i++) {
      w(i)=opt_param(i+lambda.size());
    }
    for (int i = 0; i < taskSel.size(); i++) {
      taskSel(i)=opt_param(i+lambda.size()+w.size());
      // taskSel has now a continous value among 0 and 1. Discretize in 3 binary task activators
      if (i==1 || i==3 || i==4 || i==5|| i==6){ 
        if(taskSel(i)<0.26){
          taskAct_1(i) = 1;
          taskAct_2(i) = 0;
          taskAct_3(i) = 0;
        }
        else if(taskSel(i)<0.76){
          taskAct_1(i) = 0;
          taskAct_2(i) = 1;
          taskAct_3(i) = 0;
        }
        else{
          taskAct_1(i) = 0;
          taskAct_2(i) = 0;
          taskAct_3(i) = 1;
        }
      }
      else{
        if(taskSel(i)<0.38){
          taskAct_1(i) = 1;
          taskAct_2(i) = 0;
          taskAct_3(i) = 0;
        }
        else {
          taskAct_1(i) = 0;
          taskAct_2(i) = 1;
          taskAct_3(i) = 0;
        }
      }
    }
}


void control::icub_learning_controller::reset() {

  LOG_INFO("Setting model:");

   LOG_INFO("Setting model:");
  std::list<unsigned int> waist1_id, waist2_id, com_id, position_id, orientation_id, foot1_id, foot2_id;
  std::list<unsigned int> head_id, torso_id, l_arm_id, r_arm_id, l_leg_id, r_leg_id;

  base_frame_.p.x(q_(0));
  base_frame_.p.y(q_(1));
  base_frame_.p.z(q_(2));
  base_frame_.M = base_frame_.M.RPY(q_(3), q_(4), q_(5));

  model_ptr()->setFloatingBasePose(base_frame_);
  model_ptr()->setJointPosition(q0_);
  model_ptr()->setJointVelocity(dq0_);
  // model_ptr()->setJointAcceleration(ddq_);
  model_ptr()->update();


  // init tasks
  // left_foot.reset(new OpenSoT::tasks::velocity::Cartesian("left_foot",
  //  q0_, *(model_ptr_.get()), "left_foot", "world"));
  // left_foot->setLambda(lambda(0));
  // left_foot->setOrientationErrorGain(lambda(1));

  // right_foot.reset(new OpenSoT::tasks::velocity::Cartesian("right_foot",
  //  q0_, *(model_ptr_.get()), "right_foot", "world"));
  // right_foot->setLambda(lambda(0));
  // right_foot->setOrientationErrorGain(lambda(1));

    left_foot_1.reset(new OpenSoT::tasks::velocity::Cartesian("left_foot",
   q0_, *(model_ptr_.get()), "left_foot", "world"));
  left_foot_1->setLambda(lambda(0));
  left_foot_1->setOrientationErrorGain(lambda(1));

  right_foot_1.reset(new OpenSoT::tasks::velocity::Cartesian("right_foot",
   q0_, *(model_ptr_.get()), "right_foot", "world"));
  right_foot_1->setLambda(lambda(0));
  right_foot_1->setOrientationErrorGain(lambda(1));

  left_foot_2.reset(new OpenSoT::tasks::velocity::Cartesian("left_foot",
   q0_, *(model_ptr_.get()), "left_foot", "world"));
  left_foot_2->setLambda(lambda(0));
  left_foot_2->setOrientationErrorGain(lambda(1));

  right_foot_2.reset(new OpenSoT::tasks::velocity::Cartesian("right_foot",
   q0_, *(model_ptr_.get()), "right_foot", "world"));
  right_foot_2->setLambda(lambda(0));
  right_foot_2->setOrientationErrorGain(lambda(1));

  com_.reset(new OpenSoT::tasks::velocity::CoM(q0_, *model_ptr_));
  com_->setLambda(lambda(2));

  waist_.reset(new OpenSoT::tasks::velocity::Cartesian("waist",
   q0_, *(model_ptr_.get()), "waist", "world"));
  waist_->setLambda(lambda(3));
  
  head_.reset(new OpenSoT::tasks::velocity::Cartesian("head",
   q0_, *(model_ptr_.get()), "head", "world"));
  head_->setOrientationErrorGain(lambda(4));

  postural_.reset(new OpenSoT::tasks::velocity::Postural(q0_));
  postural_->setLambda(lambda(5));

  // set constraints (joint and velocity limits)
  Eigen::VectorXd qmin, qmax;
  model_ptr_->getJointLimits(qmin, qmax);
  joint_lims_.reset(new OpenSoT::constraints::velocity::JointLimits(q0_,
   qmax, qmin));


  //LOG_INFO("Checking joints limits");
  for (int i = 0; i < q_.size(); ++i) {
    if(q_(i) < qmin(i)) {
      //succ = false;
      LOG_WARNING("q(" << i << ") inferior to the joint limit q = " <<
        q_(i) << " qmin = " << qmin(i));
    } else if(q_(i) > qmax(i)) {
      //succ = false;
      LOG_WARNING("q(" << i << ") superior to the joint limit q = " <<
        q_(i) << " qmax = " << qmax(i));
    } else {
      LOG_SUCCESS("q(" << i << ") = " << q_(i) << " OK");
    }
  }
  //LOG_INFO("--Joints' limits initialized");

  vel_lims_.reset(new OpenSoT::constraints::velocity::VelocityLimits(M_PI_4*2,
   dT_, q0_.size()));
  com_init_pose_ = eigenToKDL(com_->getActualPosition());
  model_ptr_->getPose("right_foot", "world", right_foot_init_pose_); 
  model_ptr_->getPose("left_foot", "world", left_foot_init_pose_);
  baseInitPose = waist_->getActualPose();
  baseInitPosition = baseInitPose.block<3,1>(0,3);
  headInitPose = head_->getActualPose();
  CoM_ = com_->getActualPosition();
  q_init = postural_->getReference(); 
  // rFootInitPose = right_foot->getActualPose();
  // lFootInitPose = left_foot->getActualPose();
  rFootInitPose = right_foot_1->getActualPose();
  lFootInitPose = left_foot_1->getActualPose();
  //initial distance between measured foot pose frame and com position


  com_id.push_back(0);
  com_id.push_back(1);
  waist1_id.push_back(2);

  position_id.push_back(0);
  position_id.push_back(1);
  position_id.push_back(2);
  orientation_id.push_back(3);
  orientation_id.push_back(4);
  orientation_id.push_back(5);


  head_id.push_back(model_ptr_->getDofIndex("neck_pitch"));
  head_id.push_back(model_ptr_->getDofIndex("neck_roll"));
  head_id.push_back(model_ptr_->getDofIndex("neck_yaw"));

  torso_id.push_back(model_ptr_->getDofIndex("torso_pitch"));
  torso_id.push_back(model_ptr_->getDofIndex("torso_roll"));
  torso_id.push_back(model_ptr_->getDofIndex("torso_yaw"));

  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_pitch"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_roll"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_yaw"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_elbow_joint"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_prosup"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_pitch"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_yaw"));
  
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_pitch"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_roll"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_yaw"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_elbow_joint"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_prosup"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_pitch"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_yaw"));

  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_pitch"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_roll"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_yaw"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_knee"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_ankle_pitch"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_ankle_roll"));
  
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_pitch"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_roll"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_yaw"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_knee"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_ankle_pitch"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_ankle_roll"));


  // OpenSoT::SubTask::Ptr right_foot_p_sub_1(new OpenSoT::SubTask(right_foot, position_id));
  // OpenSoT::SubTask::Ptr right_foot_p_sub_2(new OpenSoT::SubTask(right_foot, position_id));
  // OpenSoT::SubTask::Ptr right_foot_o_sub_1(new OpenSoT::SubTask(right_foot, orientation_id));
  // OpenSoT::SubTask::Ptr right_foot_o_sub_2(new OpenSoT::SubTask(right_foot, orientation_id));

  // OpenSoT::SubTask::Ptr left_foot_p_sub_1(new OpenSoT::SubTask(left_foot, position_id));
  // OpenSoT::SubTask::Ptr left_foot_p_sub_2(new OpenSoT::SubTask(left_foot, position_id));
  // OpenSoT::SubTask::Ptr left_foot_o_sub_1(new OpenSoT::SubTask(left_foot, orientation_id));
  // OpenSoT::SubTask::Ptr left_foot_o_sub_2(new OpenSoT::SubTask(left_foot, orientation_id));

  OpenSoT::SubTask::Ptr waist1_sub_1(new OpenSoT::SubTask(waist_, waist1_id));
  OpenSoT::SubTask::Ptr waist1_sub_2(new OpenSoT::SubTask(waist_, waist1_id));
  OpenSoT::SubTask::Ptr waist1_sub_3(new OpenSoT::SubTask(waist_, waist1_id));

  OpenSoT::SubTask::Ptr com_sub_1(new OpenSoT::SubTask(com_, com_id));
  OpenSoT::SubTask::Ptr com_sub_2(new OpenSoT::SubTask(com_, com_id));
  OpenSoT::SubTask::Ptr com_sub_3(new OpenSoT::SubTask(com_, com_id));

  OpenSoT::SubTask::Ptr headj_sub_1(new OpenSoT::SubTask(postural_, head_id));
  OpenSoT::SubTask::Ptr l_armj_sub_1(new OpenSoT::SubTask(postural_, l_arm_id));
  OpenSoT::SubTask::Ptr r_armj_sub_1(new OpenSoT::SubTask(postural_, r_arm_id));
  OpenSoT::SubTask::Ptr torsoj_sub_1(new OpenSoT::SubTask(postural_, torso_id));
  OpenSoT::SubTask::Ptr l_legj_sub_1(new OpenSoT::SubTask(postural_, l_leg_id));
  OpenSoT::SubTask::Ptr r_legj_sub_1(new OpenSoT::SubTask(postural_, r_leg_id));
  OpenSoT::SubTask::Ptr headj_sub_2(new OpenSoT::SubTask(postural_, head_id));
  OpenSoT::SubTask::Ptr l_armj_sub_2(new OpenSoT::SubTask(postural_, l_arm_id));
  OpenSoT::SubTask::Ptr r_armj_sub_2(new OpenSoT::SubTask(postural_, r_arm_id));
  OpenSoT::SubTask::Ptr torsoj_sub_2(new OpenSoT::SubTask(postural_, torso_id));
  OpenSoT::SubTask::Ptr headj_sub_3(new OpenSoT::SubTask(postural_, head_id));
  OpenSoT::SubTask::Ptr l_armj_sub_3(new OpenSoT::SubTask(postural_, l_arm_id));
  OpenSoT::SubTask::Ptr r_armj_sub_3(new OpenSoT::SubTask(postural_, r_arm_id));
  OpenSoT::SubTask::Ptr torsoj_sub_3(new OpenSoT::SubTask(postural_, torso_id));


  OpenSoT::SubTask::Ptr head_sub_1(new OpenSoT::SubTask(head_, orientation_id));
  OpenSoT::SubTask::Ptr head_sub_2(new OpenSoT::SubTask(head_, orientation_id));
  OpenSoT::SubTask::Ptr head_sub_3(new OpenSoT::SubTask(head_, orientation_id));



bool empty_3 = true;
  taskAct_3.setZero();
  for (int i=0; i<taskAct_3.size(); i++){
    if (taskAct_3(i)==0)
      empty_3 = empty_3 && true;
    else {
      empty_3 = false;
      break;
    }
  }
  // if (empty_3){
  //   auto_stack_ = (w(0)*right_foot_p_sub_1 + w(7)*right_foot_o_sub_1 + w(0)*left_foot_p_sub_1 + w(7)*left_foot_o_sub_1 + w(1)*waist1_sub_1  + w(2)*com_sub_1 + w(3)*head_sub_1 + w(4)*headj_sub_1 + w(5)*torsoj_sub_1 + w(6)*l_armj_sub_1 + w(6)*r_armj_sub_1)/
  //               (w(0)*right_foot_p_sub_2 + w(7)*right_foot_o_sub_2 + w(0)*left_foot_p_sub_2 + w(7)*left_foot_o_sub_2 + w(2)*com_sub_2 + w(3)*head_sub_2 + w(4)*headj_sub_2 + w(5)*torsoj_sub_2 + w(6)*l_armj_sub_2 + w(6)*r_armj_sub_2);
  // }
  // else{
  //   auto_stack_ = (w(0)*right_foot_p_sub_1 + w(7)*right_foot_o_sub_1 + w(0)*left_foot_p_sub_1 + w(7)*left_foot_o_sub_1 + w(1)*waist1_sub_1 + w(2)*com_sub_1 + w(3)*head_sub_1 + w(4)*headj_sub_1 + w(5)*torsoj_sub_1 + w(6)*l_armj_sub_1 + w(6)*r_armj_sub_1)/
  //               (w(0)*right_foot_p_sub_2 + w(7)*right_foot_o_sub_2 + w(0)*left_foot_p_sub_2 + w(7)*left_foot_o_sub_2 + w(1)*waist1_sub_2 + w(2)*com_sub_2 + w(3)*head_sub_2 + w(4)*headj_sub_2 + w(5)*torsoj_sub_2 + w(6)*l_armj_sub_2 + w(6)*r_armj_sub_2)/
  //               (w(1)*waist1_sub_3 + w(2)*com_sub_3 + w(3)*head_sub_3 + w(4)*headj_sub_3 + w(5)*torsoj_sub_3 + w(6)*l_armj_sub_3 + w(6)*r_armj_sub_3);
  // }
  if (empty_3){
    auto_stack_ = (w(0)*left_foot_1 + w(0)*right_foot_1 + w(1)*waist1_sub_1  + w(2)*com_sub_1 + w(3)*head_sub_1 + w(4)*headj_sub_1 + w(5)*torsoj_sub_1 + w(6)*l_armj_sub_1 + w(6)*r_armj_sub_1)/
                (w(0)*left_foot_2 + w(0)*right_foot_2 + w(1)*waist1_sub_2 + w(2)*com_sub_2 + w(3)*head_sub_2 + w(4)*headj_sub_2 + w(5)*torsoj_sub_2 + w(6)*l_armj_sub_2 + w(6)*r_armj_sub_2);
  }
  else{
    auto_stack_ = (w(0)*left_foot_1 + w(0)*right_foot_1 + w(1)*waist1_sub_1 + w(2)*com_sub_1 + w(3)*head_sub_1 + w(4)*headj_sub_1 + w(5)*torsoj_sub_1 + w(6)*l_armj_sub_1 + w(6)*r_armj_sub_1)/
                (w(0)*left_foot_2 + w(0)*right_foot_2 + w(1)*waist1_sub_2 + w(2)*com_sub_2 + w(3)*head_sub_2 + w(4)*headj_sub_2 + w(5)*torsoj_sub_2 + w(6)*l_armj_sub_2 + w(6)*r_armj_sub_2)/
                (w(1)*waist1_sub_3 + w(2)*com_sub_3 + w(3)*head_sub_3 + w(4)*headj_sub_3 + w(5)*torsoj_sub_3 + w(6)*l_armj_sub_3 + w(6)*r_armj_sub_3);
  }

  // right_foot_p_sub_1->setActive(taskAct_1(0));
  // right_foot_o_sub_1->setActive(taskAct_1(7));
  // left_foot_p_sub_1->setActive(taskAct_1(0));
  // left_foot_o_sub_1->setActive(taskAct_1(7));
  left_foot_1->setActive(taskAct_1(0));
  right_foot_1->setActive(taskAct_1(0));
  waist1_sub_1->setActive(taskAct_1(1));
  com_sub_1->setActive(taskAct_1(2));
  head_sub_1->setActive(taskAct_1(3));
  headj_sub_1->setActive(taskAct_1(4));
  torsoj_sub_1->setActive(taskAct_1(5));
  l_armj_sub_1->setActive(taskAct_1(6));
  r_armj_sub_1->setActive(taskAct_1(6));

  // right_foot_p_sub_2->setActive(taskAct_1(0));
  // right_foot_o_sub_2->setActive(taskAct_1(7));
  // left_foot_p_sub_2->setActive(taskAct_1(0));
  // left_foot_o_sub_2->setActive(taskAct_1(7));
  left_foot_2->setActive(taskAct_2(0));
  right_foot_2->setActive(taskAct_2(0));
  waist1_sub_2->setActive(taskAct_2(1));
  com_sub_2->setActive(taskAct_2(2));
  head_sub_2->setActive(taskAct_2(3));
  headj_sub_2->setActive(taskAct_2(4));
  torsoj_sub_2->setActive(taskAct_2(5));
  l_armj_sub_2->setActive(taskAct_2(6));
  r_armj_sub_2->setActive(taskAct_2(6));

  waist1_sub_3->setActive(taskAct_3(1));
  com_sub_3->setActive(taskAct_3(2));
  head_sub_3->setActive(taskAct_3(3));
  headj_sub_3->setActive(taskAct_3(4));
  torsoj_sub_3->setActive(taskAct_3(5));
  l_armj_sub_3->setActive(taskAct_3(6));
  r_armj_sub_3->setActive(taskAct_3(6));

  auto_stack_ = auto_stack_  << joint_lims_ << vel_lims_ << convex_hull_;
  //=======================================
  //// Save parameters 
  //ctrl_Param << lambda, w, taskAct_1, taskAct_2, taskAct_3;

  //  init solver
  auto_stack_->update(q0_);
  //LOG_INFO("--AutoStack set");
  solver_.reset(new OpenSoT::solvers::iHQP(auto_stack_->getStack(),
  auto_stack_->getBounds(), 1e8,
  OpenSoT::solvers::solver_back_ends::qpOASES));
  //LOG_INFO("--Solver reset");


  end_sequence = false;
  timeExpired = false;
  stepsCount = 0;
  SM()->setState("balance_com_on_left_foot");
     
  setStateMachine();
}


void control::icub_learning_controller::setStateMachine() {

  SM()->add("balance_com_on_left_foot", [this]() {
    if (SM()->firstRun()) {
      std::list<std::string> linksInContact;
      linksInContact.push_back("fr_right_foot");
      linksInContact.push_back("br_right_foot");
      linksInContact.push_back("fl_left_foot");
      linksInContact.push_back("bl_left_foot");
      convex_hull_->setLinksInContact(linksInContact);

      if(taskSel(0)<0.38){
        left_foot_1->getActualPose(f_init);
        right_foot_1->getActualPose(f_lift);
      } else {
        left_foot_2->getActualPose(f_init);
        right_foot_2->getActualPose(f_lift);
      }

      head_->getActualPose(f_head);
      CoM_ = com_->getActualPosition();
      updateReferences(-y_com_d, x_foot_d, y_foot_d, z_foot_d, true);

      duration = 0;
      SM()->setFirstrun(false);
    }

    // LOG_INFO("In idle state");
    if (com_->getError().norm() < 0.01) {
      KDL::Frame T_ref_com = trjGeneratorC_->Pos();
      comReference = frameToVec(T_ref_com);
      com_->setReference(comReference, Eigen::Vector3d(0.0, 0.0, 0.0));
      trjGeneratorC_->updateTrj();
      KDL::Frame T_ref_foot = trjGeneratorF_->Pos();
      rightReference = frameToVec(T_ref_foot);

      current_time = trjGeneratorF_->getTime();
      // if (current_time > 99/100 * time_2){
      //   std::list<std::string> linksInContact;
      //   linksInContact.push_back("fr_left_foot");
      //   linksInContact.push_back("br_left_foot");
      //   linksInContact.push_back("fl_left_foot");
      //   linksInContact.push_back("bl_left_foot");
      //   convex_hull_->setLinksInContact(linksInContact);
      // }

      if(taskSel(0)<0.38){
        footPose = left_foot_1->getActualPose();
        stepOnceTrj(trjGeneratorF_, right_foot_1);
      } else {
        footPose = left_foot_2->getActualPose();
        stepOnceTrj(trjGeneratorF_, right_foot_2);
      }
      leftReference = footPose.block<3,1>(0,3);
      footReference.head(3)=rightReference;
      footReference.tail(3)=leftReference;

      trjGeneratorN_->updateTrj();
      if (trjGeneratorF_->isFinished() && trjGeneratorC_->isFinished()) { 
        SM()->setState("rest_right");
      }
    }       
    duration++;
    if (duration>duration_limit) {
      timeExpired = true;
      SM()->setState("done");
    }

    bool res = solve();
    return res;
  });

  SM()->add("balance_com_on_right_foot", [this]() {
    if (SM()->firstRun()) {
      std::list<std::string> linksInContact;
      linksInContact.push_back("fr_right_foot");
      linksInContact.push_back("br_right_foot");
      linksInContact.push_back("fl_left_foot");
      linksInContact.push_back("bl_left_foot");
      convex_hull_->setLinksInContact(linksInContact);

      if(taskSel(0)<0.38){
        left_foot_1->getActualPose(f_lift);
        right_foot_1->getActualPose(f_init);
      } else {
        left_foot_2->getActualPose(f_lift);
        right_foot_2->getActualPose(f_init);
      }
      head_->getActualPose(f_head);
      CoM_ = com_->getActualPosition();
      updateReferences(y_com_d, x_foot_d, -y_foot_d, z_foot_d, true);

      duration = 0;
      SM()->setFirstrun(false);
    }
    //LOG_INFO("In idle state");
    if (com_->getError().norm() < 0.01) {
      KDL::Frame T_ref_com = trjGeneratorC_->Pos();
      comReference = frameToVec(T_ref_com);
      com_->setReference(comReference, Eigen::Vector3d(0.0, 0.0, 0.0));
      trjGeneratorC_->updateTrj();

      KDL::Frame T_ref_foot = trjGeneratorF_->Pos();
      leftReference = frameToVec(T_ref_foot);

      current_time = trjGeneratorF_->getTime();
      // if (current_time > 9/10 * time_2){
      //   std::list<std::string> linksInContact;
      //   linksInContact.push_back("fr_right_foot");
      //   linksInContact.push_back("br_right_foot");
      //   linksInContact.push_back("fl_right_foot");
      //   linksInContact.push_back("bl_right_foot");
      //   convex_hull_->setLinksInContact(linksInContact);
      // }

      if(taskSel(0)<0.38){
        footPose = right_foot_1->getActualPose();
        stepOnceTrj(trjGeneratorF_, left_foot_1);
      } else {
        footPose = right_foot_2->getActualPose();
        stepOnceTrj(trjGeneratorF_, left_foot_2);
      }

      rightReference = footPose.block<3,1>(0,3);
      footReference.head(3)=rightReference;
      footReference.tail(3)=leftReference;

      trjGeneratorN_->updateTrj();
      if (trjGeneratorF_->isFinished() && trjGeneratorC_->isFinished()) {
          SM()->setState("rest_left");
      }
    }
    duration++;
    if (duration>duration_limit) {
      timeExpired = true;
      SM()->setState("done");
    }

    bool res = solve();
    return res;
  });

  SM()->add("rest_right", [this]() {
    if (SM()->firstRun()) {

      std::list<std::string> linksInContact;
      linksInContact.push_back("fr_right_foot");
      linksInContact.push_back("br_right_foot");
      linksInContact.push_back("fl_right_foot");
      linksInContact.push_back("bl_right_foot");
      convex_hull_->setLinksInContact(linksInContact);

      if(taskSel(0)<0.38){
        left_foot_1->getActualPose(f_init);
        right_foot_1->getActualPose(f_lift);
      } else {
        left_foot_2->getActualPose(f_init);
        right_foot_2->getActualPose(f_lift);
      }
        CoM_ = com_->getActualPosition();
        updateReferences(0.0, x_foot_d, -y_foot_d, -z_foot_d, false);

        duration = 0;
        SM()->setFirstrun(false);
    }
    //LOG_INFO("In idle state");
    if (com_->getError().norm() < 0.01) {
      KDL::Frame T_ref_com = trjGeneratorC_->Pos();
      comReference = frameToVec(T_ref_com);
      com_->setReference(comReference, Eigen::Vector3d(0.0, 0.0, 0.0));
      trjGeneratorC_->updateTrj();

      KDL::Frame T_ref_foot = trjGeneratorF_->Pos();
      rightReference = frameToVec(T_ref_foot);

      current_time = trjGeneratorF_->getTime();
      if (current_time > 1/4 * time_1){
        std::list<std::string> linksInContact;
        linksInContact.push_back("fr_right_foot");
        linksInContact.push_back("br_right_foot");
        linksInContact.push_back("fl_left_foot");
        linksInContact.push_back("bl_left_foot");
        convex_hull_->setLinksInContact(linksInContact);
      }
      
      if(taskSel(0)<0.38){
        footPose = left_foot_1->getActualPose();
        stepOnceTrj(trjGeneratorF_, right_foot_1);
      } else {
        footPose = left_foot_2->getActualPose();
        stepOnceTrj(trjGeneratorF_, right_foot_2);
      }
      
      leftReference = footPose.block<3,1>(0,3);
      footReference.head(3)=rightReference;
      footReference.tail(3)=leftReference;

      trjGeneratorN_->updateTrj();
      if (trjGeneratorC_->isFinished() && trjGeneratorF_->isFinished()) {
          stepsCount ++;
          if (stepsCount<10){
            SM()->setState("balance_com_on_right_foot");
          } else {
            SM()->setState("done");
          }
      }
    }

    duration++;
    if (duration>duration_limit) {
      timeExpired = true;
      SM()->setState("done");
    }

    bool res = solve();
    return res;
  });

  SM()->add("rest_left", [this]() {
    if (SM()->firstRun()) {
      
      std::list<std::string> linksInContact;
      linksInContact.push_back("fr_left_foot");
      linksInContact.push_back("br_left_foot");
      linksInContact.push_back("fl_left_foot");
      linksInContact.push_back("bl_left_foot");
      convex_hull_->setLinksInContact(linksInContact);
      
      if(taskSel(0)<0.38){
        left_foot_1->getActualPose(f_lift);
        right_foot_1->getActualPose(f_init);
      } else {
        left_foot_2->getActualPose(f_lift);
        right_foot_2->getActualPose(f_init);
      }
      CoM_ = com_->getActualPosition();
      updateReferences(0.0, x_foot_d, y_foot_d, -z_foot_d, false);

      duration = 0;
      SM()->setFirstrun(false);
    }

    //LOG_INFO("In idle state");
    if (com_->getError().norm() < 0.01) {
      KDL::Frame T_ref_com = trjGeneratorC_->Pos();
      comReference = frameToVec(T_ref_com);
      com_->setReference(comReference, Eigen::Vector3d(0.0, 0.0, 0.0));
      trjGeneratorC_->updateTrj();

      KDL::Frame T_ref_foot = trjGeneratorF_->Pos();
      leftReference = frameToVec(T_ref_foot);

      current_time = trjGeneratorF_->getTime();
      if (current_time > 1/4 * time_1){
        std::list<std::string> linksInContact;
        linksInContact.push_back("fr_right_foot");
        linksInContact.push_back("br_right_foot");
        linksInContact.push_back("fl_left_foot");
        linksInContact.push_back("bl_left_foot");
        convex_hull_->setLinksInContact(linksInContact);
      }

      if(taskSel(0)<0.38){
        footPose = right_foot_1->getActualPose();
        stepOnceTrj(trjGeneratorF_, left_foot_1);
      } else {
        footPose = right_foot_2->getActualPose();
        stepOnceTrj(trjGeneratorF_, left_foot_2);
      }

      rightReference = footPose.block<3,1>(0,3);
      footReference.head(3)=rightReference;
      footReference.tail(3)=leftReference;

      trjGeneratorN_->updateTrj();
      if (trjGeneratorC_->isFinished() && trjGeneratorF_->isFinished()) {
        stepsCount ++;
        if (stepsCount<10){
          SM()->setState("balance_com_on_left_foot");
        } else {
          SM()->setState("done");
        }
      }
    }

    duration++;
    if (duration>duration_limit) {
      timeExpired = true;
      SM()->setState("done");
    }

    bool res = solve();
    return res;
  });

  SM()->add("done", [this]() {
    if (SM()->firstRun()) {
      if (timeExpired==false){
        LOG_INFO("Gait Complete");
      } else {
        LOG_INFO("Out of Time");
      }
      end_sequence = true;
      //Do nothing
      SM()->setFirstrun(false);
    }
    bool res = solve();
    return res;
  });

}

bool control::icub_learning_controller::run() {  
  return ((*SM()) ());
}


//::::::::::::::::::::::::::::::::: TELEOPERATION FUNCTIONS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void control::icub_learning_controller::updateReferences(double y_c, double x_f, double y_f, double z_f, bool isLifting) {  
      
      if (isLifting == true){
        f_goal.p.x(f_init.p.x()); // forward
        f_goal.p.y(f_lift.p.y() + y_f); // leftward
        f_goal.p.z(f_lift.p.z() + z_f); // upward
        f_goal.M = f_lift.M;
        
        new_com = CoM_;
        new_com(0) = f_init.p.x();
        new_com(1) = f_init.p.y() + y_c;

        com_pose.p.x(CoM_(0));
        com_pose.p.y(CoM_(1));
        com_pose.p.z(CoM_(2));
        com_target.p.x(new_com(0));
        com_target.p.y(new_com(1));
        com_target.p.z(new_com(2));

      f_head_goal.p.x(f_head.p.x()); // forward
      f_head_goal.p.y(0.0); // leftward
      f_head_goal.p.z(f_head.p.z()); // upward
      f_head_goal.M = f_head.M;
        
      computeTrj(trjGeneratorF_, f_lift, f_goal, time_2);
      computeTrj(trjGeneratorN_, f_head, f_head_goal, time_1);
      computeTrj(trjGeneratorC_, com_pose, com_target, time_1);

      } else {
        f_goal.p.x(f_lift.p.x() + x_f); // forward
        f_goal.p.y(f_lift.p.y() + y_f); // leftward
        f_goal.p.z(f_lift.p.z() + z_f); // upward
        f_goal.M = f_lift.M;
        
        new_com = CoM_;
        new_com(0) = (f_init.p.x() + f_lift.p.x())/2;
        new_com(1) = 0.0;

        com_pose.p.x(CoM_(0));
        com_pose.p.y(CoM_(1));
        com_pose.p.z(CoM_(2));
        com_target.p.x(new_com(0));
        com_target.p.y(new_com(1));
        com_target.p.z(new_com(2));

        f_head_goal.p.x(f_head.p.x()); // forward
        f_head_goal.p.y(0.0); // leftward
        f_head_goal.p.z(f_head.p.z()); // upward
        f_head_goal.M = f_head.M;


      computeTrj(trjGeneratorF_, f_lift, f_goal, time_1);
      computeTrj(trjGeneratorN_, f_head, f_head_goal, time_1);
      computeTrj(trjGeneratorC_, com_pose, com_target, time_2);

      }





}

Eigen::Vector3d control::icub_learning_controller::frameToVec(KDL::Frame f) {  

  Eigen::Vector3d vec;
  vec(0) = f.p.x();
  vec(1) = f.p.y();
  vec(2) = f.p.z();

  return vec;
}