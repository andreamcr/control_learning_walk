#include "env/robotDart.hpp"
#include <iostream>
#include <fstream>

env::RobotDart::RobotDart(std::string configFile, double dt, std::string robotName, std::string urdf,
    std::string packageName, std::string packagePath, Eigen::VectorXd q0, 
    Eigen::VectorXd dq0, Eigen::VectorXf param):
    dt_(dt),
    robotName_(robotName),
    urdf_(urdf), 
    packageName_(packageName),
    packagePath_(packagePath),
    q0_(q0),
    dq0_(dq0), 
    configFile_(configFile),
    param_(param)
{
    // initialize fitness w/ worst score. AUTOSTACK FAILURE
    fitness_(0) = 100000.0;  // tracking error
    fitness_(1) = 100000.0;  // torques
    //LOG_SUCCESS("Environment initialized");
}

env::RobotDart::~RobotDart(){
    stop_ = true;
    //LOG_INFO("Destroying environment");
}

void env::RobotDart::initQPController(){
    // Instantiate your controller and then downcast it
    loadController("/home/icubuser/control_learning/build/libicub_controller.so");
    qpController_ = api::Factory::instance().createController("icub_learning_DS", configFile_, dt_);
    //qpController_ = std::make_shared<control::icub_learning_controller>(configFile_, dt_);
    my_qpController = std::static_pointer_cast<control::icub_learning_controller>(qpController_);

    qpController_->openLoop(true);
    qpController_->feedbackFeetWrench(false);
    qpController_->getModelJointNames(jNames_);
    qpController_->setInitConfiguration(q0_, dq0_);
    my_qpController->setCtrlParameters(param_);
    qpController_->reset();
    // for ( auto &s: jNames_) LOG_INFO(s);
}

void env::RobotDart::loadController(const std::string& path) {
  void* dlHandle = dlopen(path.c_str(), RTLD_LAZY | RTLD_GLOBAL);
  if (!dlHandle) {
    std::cerr << "Cannot load library: " << dlerror() << std::endl;
  }
  // reset errors
  dlerror();

  // load the symbols
  typedef void (*registerController_t)();
  registerController_t regFunc =
      (registerController_t)dlsym(dlHandle, "registerController");
  const char* dlsym_error = dlerror();
  if (dlsym_error) {
    std::cerr << "Failed to resolve registerController "
              << ": " << dlerror() << std::endl;
    dlclose(dlHandle);
  } else {
    regFunc();
  }
}

void env::RobotDart::initSimulator(bool graphics){
    // INSTANTIATE YOUR SIMULATOR
    stop_ = false;
    dartSim_ = std::make_shared<bridge::dart_robot_loader>(&stop_);

    dartSim_->load(0.005);
    dartSim_->setCollisionDetector("bullet");
    dartSim_->addRobot(robotName_,urdf_, packageName_,packagePath_);
    //dartSim_->set_step(0.01);
    
    Eigen::VectorXd stiffness(q0_.size()), damping(q0_.size());
      for (int i = 0; i < q0_.size(); ++i) {
        stiffness(i) = 300;
        damping(i) = 1;
      }
      stiffness(28) = 100;
      stiffness(29) = 100;
      stiffness(30) = 100;

    dartSim_->setGains(stiffness, damping);
    dartSim_->setJointOrder(jNames_);
    dartSim_->setInitPositions(q0_);
    dartSim_->setPositions(q0_);

    if(graphics){
        dartSim_->runGraphics();
        dartSim_->init();
    }
}

void env::RobotDart::runEnv(int csol, std::string csim){
    //LOG_INFO("Started to run simulation");
    //RESET FITNESS
    fitness_.setZero(); 
    //get World and ICub pointers
    dart::simulation::WorldPtr world = dartSim_->getWorld();
    dart::dynamics::SkeletonPtr dartICub = dartSim_->getRobotPtr();

    //get pointers to important BodyNodes from iCub skeleton
    auto headBN = dartICub->getBodyNode("head");
    auto lHandBN = dartICub->getBodyNode("l_hand");
    auto rHandBN = dartICub->getBodyNode("r_hand");
    auto waistBN = dartICub->getBodyNode("waist");
    auto lFootBN = dartICub->getBodyNode("left_foot");
    auto rFootBN = dartICub->getBodyNode("right_foot");



    // RUN LOOP
    ofstream result;
    ofstream result_trj;
    ofstream result_torques;
    ofstream result_extra;
    if (csol != -1){
        std::string sol = std::to_string(csol);
        // result.open("new_simulation_"+ csim +"/fitness_result_opt_"+sol+".txt");
        // result_trj.open("new_simulation_"+ csim +"/traj_result_opt_"+sol+".txt");
        // result_torques.open("new_simulation_"+ csim +"/torque_result_opt_"+sol+".txt");

        // result.open("hand_tuned_res/fitness_result_hand.txt");
        // result_trj.open("hand_tuned_res/traj_result_hand.txt");
        // result_torques.open("hand_tuned_res/torque_result_hand.txt");
        // result_extra.open("new_hand_tuned_res/extra_result_hand.txt");
    }








    while (my_qpController->sequenceRunning()){
                // GET EEF WORLD TRANSFORMS
        Eigen::Isometry3d headT, lHandT, rHandT, waistT, lFootT, rFootT;
        headT  = headBN->getWorldTransform(); 
        lHandT = lHandBN->getWorldTransform();
        rHandT = rHandBN->getWorldTransform();
        waistT = waistBN->getWorldTransform();
        lFootT = lFootBN->getWorldTransform();
        rFootT = rFootBN->getWorldTransform();
        Eigen::Vector2f foot_size_ = Eigen::Vector2f(0.151, 0.058);
        

        qpController_->update();
        if (qpController_->run()){
            dartSim_->updateCommand(qpController_->getCommand());
            dartSim_->run();
        } else {
            int stepNo = my_qpController->getStepNo();
            // int stepTot = my_qpController->getSequenceLen();
            // LOG_INFO("stepTot "<< stepTot);
            LOG_INFO("noRun "<< stepNo);
            fitness_(0) += 10 - stepNo;
            fitness_(1) += 10 - stepNo;
            break;
        }
       

        // GET STEP BY STEP FITNESS
        //position error on com
        com_ref = (my_qpController->getComReference()).cast <float> ();
        com_actual = (dartICub->getCOM()).cast <float> ();
        q_tracking_error_com = (com_actual.head(2) - com_ref.head(2)).cwiseAbs();

        float com_h = com_actual(2);


        //position error on foot
        foot_ref = (my_qpController->getFootReference()).cast <float> ();
        // Eigen::VectorXf foot_actual=(my_qpController->getFootActual()).cast <float> ();
        Eigen::VectorXf foot_actual(6);
        foot_actual.head(3)=(rFootT.translation()).cast <float> ();
        foot_actual.tail(3)=(lFootT.translation()).cast <float> ();      
        q_tracking_error_foot = (foot_actual - foot_ref).cwiseAbs();

        double left_support=lFootBN->getConstraintImpulse()[5];
        if (abs(left_support)>0.001){
            left_support=1;
        }
        double right_support=rFootBN->getConstraintImpulse()[5];
        if (abs(right_support)>0.001){
            right_support=1;
        }


        //orientation error on foot
        //foot_ref = ADD REFERENCE
        // foot_r_orientation_actual=(rFootT.rotation()).cast <float> ();
        // foot_l_orientation_actual=(lFootT.rotation()).cast <float> ();
        // q_tracking_error_orientation_foot = orientationError(foot_r_orientation_ref, foot_r_orientation_actual);
        // q_tracking_error_orientation_foot += orientationError(foot_l_orientation_ref, foot_l_orientation_actual);


        //orientation error on head
        head_pose_ref = (my_qpController->getHeadReference()).cast <float> ();
        head_orientation_ref = head_pose_ref.block<3,3>(0,0);
        head_orientation_actual = (headT.rotation()).cast <float> ();
        // Eigen::MatrixXf head_pose_actual = (my_qpController->getHeadActual()).cast <float> ();
        // head_orientation_actual = head_pose_actual.block<3,3>(0,0);
        q_tracking_error_orientation_head = orientationError(head_orientation_ref, head_orientation_actual);

        Eigen::VectorXd head_position_actual = headT.translation();
        double head_y = head_position_actual(1);


        
        float q_terr_tot = 0;
        float q_terr_tot_r_foot = 0;
        float q_terr_tot_l_foot = 0;
        float q_terr_tot_o_foot = 0;
        float q_terr_tot_com = 0;
        float q_terr_tot_head = 0;
    
        for (int i=0; i<q_tracking_error_foot.size(); i++){
            q_terr_tot += q_tracking_error_foot(i);
            if (i<3) {
                q_terr_tot_r_foot+=q_tracking_error_foot(i);
            } else {
                q_terr_tot_l_foot+=q_tracking_error_foot(i);
            }
        }
        // for (int i=0; i<q_tracking_error_orientation_foot.size(); i++){
        //     //q_terr_tot += q_tracking_error_orientation_foot(i);
        //     q_terr_tot_o_foot+= q_tracking_error_orientation_foot(i);
        // }
        for (int i=0; i<q_tracking_error_com.size(); i++){
            q_terr_tot += q_tracking_error_com(i);
            q_terr_tot_com+= q_tracking_error_com(i);
        }
        for (int i=0; i<q_tracking_error_orientation_head.size(); i++){
            if (!isnan(q_tracking_error_orientation_head(i))){
                //q_terr_tot += q_tracking_error_orientation_head(i);
                q_terr_tot_head+= q_tracking_error_orientation_head(i);
            }
        }

        //torque measure
        Eigen::VectorXd torques = dartICub->getForces();
        Eigen::VectorXd torqueLimitsLow = dartICub->getForceLowerLimits();
        Eigen::VectorXd torqueLimitsUp = dartICub->getForceUpperLimits();

        double torque_penality=0;
        for (int k=6; k<18; k++){
                torque_penality+=abs(torques(k));
        }

        double full_torque = 0;
        //Eigen::VectorXd torques_abs = torques.abs();
        for (int k=0; k<torques.size(); k++){
                full_torque+=abs(torques(k));
        }

        fitness_(0) += q_terr_tot/1000;
        //fitness_(1) += torque_penality/100000;
        fitness_(1) += q_terr_tot_head/1000;

        if (csol != -1){
            // result << q_terr_tot_r_foot << " " << q_terr_tot_l_foot <<" "<<q_terr_tot_com << " " << q_terr_tot_head << "\n";
            // result_trj <<  foot_actual(0) << " " <<  foot_actual(1) << " " <<  foot_actual(2) << " " << foot_actual(3) << " " <<  foot_actual(4) << " " <<  foot_actual(5) << " " <<
            //             foot_ref(0) << " " << foot_ref(1) << " " << foot_ref(2) << " " << foot_ref(3) << " " << foot_ref(4) << " " << foot_ref(5) << " " << 
            //             com_actual(0) << " " <<  com_actual(1) << " " << com_ref(0) << " " << com_ref(1) <<  " " << right_support << " " << left_support << "\n";
            // result_torques << torques(6) << " " << torques(7) << " " << torques(8) << " " << torques(9) << " " << torques(10) << " " << torques(11) << " "
            //                 << torques(12) << " " << torques(13) << " " << torques(14) << " " << torques(15) << " " << torques(16) << " " << torques(17) << " "
            //                 << torqueLimitsLow(6) << " " << torqueLimitsLow(7) << " " << torqueLimitsLow(8) << " " << torqueLimitsLow(9) << " " << torqueLimitsLow(10) << " " << torqueLimitsLow(11) << " "
            //                 << torqueLimitsLow(12) << " " << torqueLimitsLow(13) << " " << torqueLimitsLow(14) << " " << torqueLimitsLow(15) << " " << torqueLimitsLow(16) << " " << torqueLimitsLow(17) << " "
            //                 << torqueLimitsUp(6) << " " << torqueLimitsUp(7) << " " << torqueLimitsUp(8) << " " << torqueLimitsUp(9) << " " << torqueLimitsUp(10) << " " << torqueLimitsUp(11) << " "
            //                 << torqueLimitsUp(12) << " " << torqueLimitsUp(13) << " " << torqueLimitsUp(14) << " " << torqueLimitsUp(15) << " " << torqueLimitsUp(16) << " " << torqueLimitsUp(17) << "\n";
            // result_extra << head_y << " " << com_h << " " << full_torque << "\n";
        
        }
    
        //control if the robot is actual advancing
        int stepNo = my_qpController->getStepNo();
        bool outOfTime = my_qpController->checkTime();

        if (stepNo == 4 && com_actual(0) < 0.05){
            fitness_(0) += 10;
            fitness_(1) += 10;
            break;
        }
        if (isnan(fitness_(0))){
            fitness_(0) += 10 - stepNo;
            fitness_(1) += 10 - stepNo;
            break;
        }
        if (outOfTime){
            fitness_(0) += 10;
            fitness_(1) += 10;
        }

    }
}

Eigen::Vector2f env::RobotDart::getScore(){
    if (isnan(fitness_(1))|| isinf(fitness_(1))){
        fitness_(1) = 5000;
    }
    if (isnan(fitness_(0))||isinf(fitness_(0))){
        fitness_(0) = 5000;
    }
    LOG_INFO("Fitness is "<< fitness_);
    return fitness_;
}




Eigen::VectorXf env::RobotDart::orientationError(Eigen::MatrixXf rotation_A, Eigen::MatrixXf rotation_B){
    Eigen::VectorXf r(3);
    Eigen::MatrixXf rotation_AB = rotation_A*rotation_B.transpose();
    float theta = std::acos((rotation_AB.trace()-1)/2); 
    float a = 1/(2*sin(theta));
    r(0)=a*(rotation_AB(2,1)-rotation_AB(1,2));
    r(1)=a*(rotation_AB(0,2)-rotation_AB(2,0));
    r(2)=a*(rotation_AB(1,0)-rotation_AB(0,1));

    Eigen::VectorXf rot_error= (sin(theta) * r).cwiseAbs();

    return rot_error;
}