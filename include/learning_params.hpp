#ifndef CONTROL_LEARNING_PARAMS_HPP
#define CONTROL_LEARNING_PARAMS_HPP

/// Parameters for NSGA-II
struct Params {
  struct evo_float {
    SFERES_CONST float cross_rate = 0.5f;
    SFERES_CONST float mutation_rate = 0.5f;
    SFERES_CONST float eta_m = 15.0f;
    SFERES_CONST float eta_c = 10.0f;
    SFERES_CONST mutation_t mutation_type = polynomial;
    SFERES_CONST cross_over_t cross_over_type = sbx;
  };
  struct pop {
    SFERES_CONST unsigned size = 100;
    SFERES_CONST unsigned nb_gen = 250;
    SFERES_CONST int dump_period = 100;
    SFERES_CONST int initial_aleat = 1;
  };
  struct parameters {
    SFERES_CONST float min = 0.0f;
    SFERES_CONST float max = 1.0f;
  };
};

#endif
