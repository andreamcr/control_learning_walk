#ifndef CSV_READER
#define CSV_READER
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <iostream>
#include <string>
#include <stdlib.h>

/**
 * @brief      Class handling data from .csv files.
 */
class CSVReader{
private:
	std::string fileName_;
	std::string delimeter_; 
	std::vector<std::vector<std::string> > dataList_;
	std::vector<std::vector<double>> list;
public:
	CSVReader(std::string filename, std::string delm = "," ):fileName_(filename), delimeter_(delm){
	//just initialize attributes
	}

	Eigen::MatrixXd getData(){
		std::ifstream file(fileName_.c_str()); 
		if (!file.is_open()){
			std::cout<<"Error reading .csv file. Check File path."<<std::endl;
			exit(1);
			}

		std::string line = "";
		while (getline(file, line)){
			std::vector<std::string> vec;
			boost::algorithm::split(vec, line, boost::is_any_of(delimeter_));
			dataList_.push_back(vec);
			}
		file.close();	
		for(std::vector<std::string> vec : dataList_){
			std::vector<double> oneLine;
			for(std::string data : vec){
				try{ 
					oneLine.push_back(std::atof(data.c_str()));
					}
				catch (...){
					std::cerr<<"invalid elements inside "<<fileName_<<". Check if all elements are doubles."<<std::endl;
					exit(1);
					}
				}
			list.push_back(oneLine);		
		}
		Eigen::MatrixXd data(list.size(),list[0].size());
		for (int row = 0; row < list.size(); ++row){
				for (int col = 0; col < list[0].size(); ++col){
		    		data(row,col) = list[row][col];
				}
		}
		std::cout << "[SUCCESS] Sequence loaded successfully from Csv file" << std::endl;
		return data;
	}

	Eigen::VectorXd get1Ddata(int index,Eigen::MatrixXd data){
		Eigen::VectorXd data_aux(list.size());

		if (index<0 || index > data.outerSize()){
			std::cout<<"Indexing error in .csv file. Index= columns in .csv file. Indexing starts from 0."<<std::endl;
			exit(1);
		}

		for (int row = 0; row < data_aux.innerSize(); ++row){   		
	        	data_aux(row) = data(row,index) ;		
		}

		return data_aux;
	}
};

#endif