#ifndef ROBOTFRAMEWORK_CONTROL_ICUBTESTCONTROLLER_H_
#define ROBOTFRAMEWORK_CONTROL_ICUBTESTCONTROLLER_H_

//Robot Framework includes
#include "RobotFramework/control/controller.hpp"
#include "RobotFramework/control/opensot_controller.hpp"
#include "RobotFramework/api/factory.hpp"

#include <math.h>
#include "boost/bind.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include "iostream"
#include <vector>
#include <fstream>
#include <string> 
#include <boost/circular_buffer.hpp>
#include "csvReader.hpp"


namespace control {

class icub_learning_controller : public opensot_controller {
public:
    icub_learning_controller(const std::string &confFile, double dt);
    ~icub_learning_controller();

    void reset() override;
    bool run() override;
    // sets stack
    void setAutoStack();
    // sets state machine
    void setStateMachine();

    // get next reference from data loaded from the csv file
    bool sequenceRunning(){ return !end_sequence; }
    bool checkTime(){return timeExpired; }
    // Parameters handler functions
    void setCtrlParameters(const Eigen::VectorXf &opt_param);

    //methods for the state machine
    void updateReferences(double y_com, double x_foot, double y_foot, double z_foot, bool isLifting);
    Eigen::Vector3d frameToVec(KDL::Frame f);

    Eigen::VectorXd getFootReference(){ return footReference; }
    Eigen::VectorXd getComReference(){ return comReference; }
    Eigen::MatrixXd getHeadReference() { return headInitPose; }
    int getStepNo() {return stepsCount;}

private:

    std::shared_ptr<trajectory_utils::trajectory_generator> trjGeneratorC_;
    std::shared_ptr<trajectory_utils::trajectory_generator> trjGeneratorF_;
    std::shared_ptr<trajectory_utils::trajectory_generator> trjGeneratorN_;

    XBot::ModelInterface::Ptr rbdl_model_;

    // Tasks
    OpenSoT::tasks::velocity::Cartesian::Ptr left_foot;
    OpenSoT::tasks::velocity::Cartesian::Ptr right_foot;
    OpenSoT::tasks::velocity::Cartesian::Ptr left_foot_1;
    OpenSoT::tasks::velocity::Cartesian::Ptr right_foot_1;
    OpenSoT::tasks::velocity::Cartesian::Ptr left_foot_2;
    OpenSoT::tasks::velocity::Cartesian::Ptr right_foot_2;
    OpenSoT::tasks::velocity::Cartesian::Ptr waist_;
    OpenSoT::tasks::velocity::Postural::Ptr postural_;
    OpenSoT::tasks::velocity::Cartesian::Ptr head_;
    OpenSoT::tasks::velocity::Cartesian::Ptr chest_;
    OpenSoT::tasks::velocity::CoM::Ptr com_;
    
    // Limits
    OpenSoT::constraints::velocity::JointLimits::Ptr joint_lims_;
    OpenSoT::constraints::velocity::VelocityLimits::Ptr vel_lims_;
    // OpenSoT::constraints::torque::TorqueLimits::Ptr torque_lims_;

    // Frames
    KDL::Frame f_init, f_goal, f_lift, f_head, f_head_goal;
    KDL::Frame com_pose;
    KDL::Frame com_target;

    // Initial values of tasks
    KDL::Frame base_frame_;
    Eigen::VectorXd q_init;
    KDL::Vector com_init_pose_, com_stance_init_distance_;
    KDL::Frame right_foot_init_pose_, left_foot_init_pose_;
    Eigen::VectorXd baseInitPosition; 
    Eigen::MatrixXd baseInitPose;
    Eigen::MatrixXd headInitPose, chestInitPose;

    //reference vectors for fitness functions
    Eigen::MatrixXd footPose;
    Eigen::VectorXd posturalReference; 
    Eigen::Vector3d comReference;
    Eigen::Vector3d rightReference;
    Eigen::Vector3d leftReference;
    Eigen::Vector6d footReference;


    /////////////////////////////////////////////////////////
    int stepsCount;
    bool end_sequence;
    int duration;
    int duration_limit;
    bool timeExpired;
    Eigen::VectorXd com_ref_;
    Eigen::VectorXd r_foot_ref_;
    Eigen::VectorXd l_foot_ref_;
    // Initial values of tasks
    Eigen::MatrixXd lFootInitPose, rFootInitPose;
    Eigen::VectorXd lFootPosition, rFootPosition;
    Eigen::Vector3d new_com;
    double y_com_d;
    double x_foot_d;
    double y_foot_d;
    double z_foot_d;
    double current_time;
    double time_1;
    double time_2;

    //CONTROL PARAMETERS
    //Eigen::VectorXf ctrl_Param;
    // TO LEARN (28) //
    // 8 task gains
    Eigen::VectorXf lambda;
    // 10 task weights
    Eigen::VectorXf w;
    // 10 task NAND selector (input:cmaes signal, output:3 boolean task activators)
    Eigen::VectorXf taskSel; 
    //               //
    // 30 binary task activators
    Eigen::VectorXf taskAct_1;
    Eigen::VectorXf taskAct_2;
    Eigen::VectorXf taskAct_3;
    /////////////////////
};

}  // namespace control

#endif 