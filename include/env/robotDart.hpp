#ifndef ROBOT_DART
#define ROBOT_DART

#include <string>
#include <iostream>
#include <stdlib.h>
#include <limits>

#include <RobotFramework/api/robot_framework.hpp>
#include <RobotFramework/api/logging.hpp>
#include <RobotFramework/api/factory.hpp>
#include <RobotFramework/api/utils.hpp>

#include "control/icub_learning_controller.hpp"
#include <boost/thread.hpp>

namespace env{
/**
 * @brief      Class for controlling a robot inside Dart Simulation through a controller defined with the RobotFramework Class.
 */
class RobotDart
{
public:
	RobotDart(std::string configFile, double dt, std::string robotName, std::string urdf, std::string packageName, 
				std::string packagePath, Eigen::VectorXd q0, Eigen::VectorXd dq0, Eigen::VectorXf param);
	~RobotDart();

	void runEnv(int csol, std::string csim); //current_reference_traj // current_solution_tested //current_simulation_tested

    void loadController(const std::string &path);

	void initSimulator(bool graphics=true);

	void initQPController();

    bool isRobotFallen(Eigen::Isometry3d lHand, Eigen::Isometry3d rHand, Eigen::Isometry3d lFoot,
    					Eigen::Isometry3d rFoot, Eigen::Isometry3d waist);

    Eigen::Vector3d getZmpFromExternalForces();

    Eigen::Vector2f getScore();
    std::vector<float> getCtrlParam(){ return ctrl_param_; }
    Eigen::VectorXf orientationError(Eigen::MatrixXf rotation_A, Eigen::MatrixXf rotation_B);
  
private:

	std::shared_ptr<control::controller> qpController_;
	std::shared_ptr<control::icub_learning_controller> my_qpController;
	std::shared_ptr<bridge::dart_robot_loader> dartSim_;

    std::string configFile_, robotName_, urdf_, packageName_, packagePath_;
    std::vector<std::string> jNames_;
    Eigen::VectorXd q0_, dq0_;
    float dt_;
    

    //Fitness related
    Eigen::Vector2f fitness_;

    Eigen::VectorXf com_ref;
    Eigen::VectorXf com_actual;
    Eigen::VectorXf q_tracking_error_com;
    Eigen::VectorXf foot_ref;
    Eigen::VectorXf q_tracking_error_foot;
    Eigen::MatrixXf foot_r_orientation_ref;
    Eigen::MatrixXf foot_r_orientation_actual;
    Eigen::MatrixXf foot_l_orientation_ref;
    Eigen::MatrixXf foot_l_orientation_actual;
    Eigen::VectorXf q_tracking_error_orientation_foot;
    Eigen::MatrixXf head_pose_ref;
    Eigen::MatrixXf head_orientation_ref;
    Eigen::MatrixXf head_orientation_actual;
    Eigen::VectorXf q_tracking_error_orientation_head;
    

    // optimization parameters
    Eigen::VectorXf param_;
    // control parameters
    std::vector<float> ctrl_param_;

    //Performance related
    bool robot_fallen;
    bool stop_;
    std::chrono::high_resolution_clock::time_point last_t_;
};

} // end namespace env

#endif